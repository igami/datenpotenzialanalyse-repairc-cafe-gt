
# Notwendige Bibliotheken laden

library(readr)
library(ggplot2)
library(dplyr)


# Laufzettel importieren

Laufzettel_tidy <- read_csv( "Anhang/Laufzettel-tidy.csv" )

# NULL und FALSE durch NA ersetzen, damit es nicht mitgezählt wird

Laufzettel_tidy[Laufzettel_tidy == "NULL"] <- NA 
Laufzettel_tidy[Laufzettel_tidy == "FALSE"] <- NA 


# Übersicht über die Anzahl an Werten, Anzahl an unterschiedlichen Werten, und Werte pro Feld erstellen

Datenfelder_tidy <- data.frame( variable = c( NA ), length = c( NA ), unique = c( NA ), values = c( NA ) )

for ( i in 1:ncol( Laufzettel_tidy ) ){
  Datenfelder_tidy[ i, 1 ] <-    colnames(         Laufzettel_tidy )[ i ]
  Datenfelder_tidy[ i, 2 ] <-      length(         Laufzettel_tidy[ , i ][ !is.na( Laufzettel_tidy[ , i ] ) ] )
  Datenfelder_tidy[ i, 3 ] <-      length( unique( Laufzettel_tidy[ , i ][ !is.na( Laufzettel_tidy[ , i ] ) ] ) )
  Datenfelder_tidy[ i, 4 ] <- paste( sort( unique( Laufzettel_tidy[ , i ][ !is.na( Laufzettel_tidy[ , i ] ) ] ) ), collapse = "\n" )
}


# Datenfelder_raw importieren

Datenfelder_raw_tidy <- read_csv( "Anhang/Datenfelder-raw.csv")


# raw Daten kennzeichnen

names( Datenfelder_raw_tidy )[ names( Datenfelder_raw_tidy ) == "length" ] <- "length-raw"
names( Datenfelder_raw_tidy )[ names( Datenfelder_raw_tidy ) == "unique" ] <- "unique-raw"
names( Datenfelder_raw_tidy )[ names( Datenfelder_raw_tidy ) == "values" ] <- "values-raw"


# Datenfelder_raw und Datenfelder_tidy in ein dataframe überführen

Datenfelder_raw_tidy <- merge( Datenfelder_raw_tidy, Datenfelder_tidy, all = TRUE )


# Datenfelder umsortieren

Datenfelder_raw_tidy <- Datenfelder_raw_tidy %>% select(
  "variable",
  "length-raw",
  "length",
  "unique-raw",
  "unique",
  "values-raw",
  "values"
)

Datenfelder_raw_tidy <- Datenfelder_raw_tidy[ match( 
  c(
    "Laufzettel",
    "Datum",
    "Nummer",
    "Einverständnis Hausordnung",
    "Reparaturkategorie",
    "Gerät/Gegenstand",
    "Gerät/Gegenstand-raw",
    "Stromversorgung",
    "Marke",
    "Baujahr",
    "Defekt",
    "Defekt-raw",
    "repariert von",
    "repariert von mir",
    "repariert mit Hilfe",
    "Fehler gefunden",
    "Ergebnis",
    "Ergebnis Detail",
    "Beschreibung",
    "Benutzungsverbot",
    "Unterschrift Benutzungsverbot",
    "Werbung durch",
    "Feedback",
    "Name",
    "Alter",
    "Alterskategorie",
    "Geschlecht",
    "E-Mail",
    "Telefon",
    "Adresse",
    "Newsletter Repair Café",
    "Newsletter Verein",
    "Unterschrift Datenverarbeitung"
  ), 
Datenfelder_raw_tidy$variable ), ]


# Datenfelder_raw_tidy exportieren

Datenfelder_raw_tidy_export <- Datenfelder_raw_tidy %>% select(
  "variable",
  "length-raw",
  "length",
  "unique-raw",
  "unique"
)

write.csv( Datenfelder_raw_tidy_export, "Anhang/Datenfelder-raw-tidy.csv", row.names = FALSE )

