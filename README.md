<h1>Datenpotenzialanalyse des Repair Café Gütersloh</h1>


angefertig von<br>
Michael Prange<br>
(133031)


Hausarbeit für das Modul<br>
**Business Intelligence**<br>
im Master-Studiengang<br>
Management für Ingenieur- und Naturwissenschaften (MBA)<br>
des Verbundstudiums an der Fachhochschule Bielefeld,<br>
Fachbereich Wirtschaft und Gesundheit


Wintersemester 2021/22


Prüfer: Prof. Dr. Peter Hartel

[![CC BY 4.0][cc-by-image]][cc-by]<br>
Diese Hausarbeit ist unter der [Creative Commons Attribution 4.0 International License][cc-by] lizenziert.

https://codeberg.org/igami/datenpotenzialanalyse-repairc-cafe-gt

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png


# Inhaltsverzeichnis

- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Abbildungsverzeichnis](#abbildungsverzeichnis)
- [Tabellenverzeichnis](#tabellenverzeichnis)
- [1. Kapitel 1 - Einleitung](#1-kapitel-1---einleitung)
  - [1.1 Was ist ein Repair Café?](#11-was-ist-ein-repair-café)
  - [1.2 Die Geschichte des Repair Café Gütersloh](#12-die-geschichte-des-repair-café-gütersloh)
- [2. Kapitel 2 - R und der Ablauf einer Datenanalyse](#2-kapitel-2---r-und-der-ablauf-einer-datenanalyse)
- [3. Kapitel 3 - Datenaufbereitung](#3-kapitel-3---datenaufbereitung)
  - [3.1 Import](#31-import)
  - [3.2 Tidy](#32-tidy)
  - [3.3 Transform](#33-transform)
- [4. Kapitel 4 - Beantwortung der Eingangsfragen](#4-kapitel-4---beantwortung-der-eingangsfragen)
  - [4.1 Wie lassen sich die vorhandenen Daten aufbereiten?](#41-wie-lassen-sich-die-vorhandenen-daten-aufbereiten)
  - [4.2 Gibt es unnütze Daten oder fehlen Daten?](#42-gibt-es-unnütze-daten-oder-fehlen-daten)
  - [4.3 Welche Informationen lassen sich daraus ableiten?](#43-welche-informationen-lassen-sich-daraus-ableiten)
- [5. Kapitel 5 - Fazit](#5-kapitel-5---fazit)
- [Anhang](#anhang)
- [Literatur- und Quellenverzeichnis](#literatur--und-quellenverzeichnis)
- [Versicherung](#versicherung)


# Abbildungsverzeichnis

- **Abb. 1: iFixit - Reparaturmanifest**  
- **Abb. 2: Schritte einer Datenanalyse**  
- **Abb. 3: Laufzettel - repaircafe.org**  
- **Abb. 4: Laufzettel - reparatur-initiativen.de**  
- **Abb. 5: Laufzettel - reparatur-initiativen.de ohne Personendaten**  
- **Abb. 6: Laufzettel - makerspace-gt**  
- **Abb. 7: Laufzettel - makerspace-gt Forum**  
- **Abb. 8: Laufzettel - reparatur-initiativen.de online**  


# Tabellenverzeichnis

- **Tab. 1: Termine des Repair Café Gütersloh**
- **Tab. 2: Datenfelder - raw/tidy**
- **Tab. 3: Formalien**


# 1. Kapitel 1 - Einleitung

Einmal im Monat veranstaltet der Makerspace Gütersloh ein Repair Café. Dabei wird versucht defekte Alltagsgegenstände zu reparieren. Zu jedem Gegenstand fallen verschiedenen Informationen wie beispielsweise Art des Gegenstandes, Beschreibung des Defekts, Erfolg der Reparatur, Alter der Gäste und wie haben die Gäste von der Veranstaltung erfahren an.

Die Daten werden auf sogenannten Laufzetteln erfasst und liegen nicht in digitaler Form vor. Bisher gab es 22 Termine mit Schätzungsweise 10 Reparaturen im Durchschnitt.

Im Rahmen der Hausarbeit sollen folgende Fragen beantwortet werden:

- Wie lassen sich die vorhandenen Daten aufbereiten?
- Gibt es unnütze Daten oder fehlen Daten?
- Welche Informationen lassen sich daraus ableiten?

Mit der Auswertung können dann Handlungsempfehlungen in Bezug auf Werbung für das Repair Café oder Weiterbildung der Ehrenamtlichen erstellt werden. Auch ohne eine Auswertung sollte eine Aufbereitung der Daten dazu führen, dass genauere Aussagen zu der Erfolgswahrscheinlichkeit von bestimmten Gegenständen geben werden können, weil sich erkennen lässt, dass Ähnliches schon öfter repariert wurde oder noch keine Erfahrung damit vorliegt.

Kapitel Eins gibt eine Einführung in die ehrenamtliche Reparatur-Bewegung im Allgemeinen und das Repair Café in Gütersloh im Speziellen. 
In Kapitel Zwei wird die Statistik-Software R und der Ablauf einer Datenanalyse vorgestellt. 
In Kapitel Drei werden die vorhandenen Daten aufbereitet und erste, einfache Auswertungen durchgeführt. 
Mit diesen Auswertungen werden in Kapitel Vier die oben genannten Fragen beantwortet. 
Abschließend fasst Kapitel Fünf die Erkenntnisse zusammen und gibt Ausblicke wie eine weitere Erhebung und Verarbeitung der Reparaturdaten erfolgen kann.

## 1.1 Was ist ein Repair Café?

**Abb. 1: iFixit - Reparaturmanifest**  
![](Abbildungsverzeichnis/iFixit%20-%20Reparaturmanifest.jpg)  
Quelle: iFixit - Reparaturmanifest (2020)

Unter den Mottos "Reparieren statt wegwerfen!" und "Wegwerfen? Denkste!" rufen die beiden Plattformen Netzwerk Reparatur-Initiativen für Deutschland und Stichting Repair Café weltweit zur Hilfe zur Selbsthilfe auf. Ein Repair Café ist eine organisierte Veranstaltung, bei der defekte Alltagsgegenstände gemeinsam von Gästen und Ehrenamtlichen untersucht und im besten Fall repariert werden. Lautet die Fehleranalyse im schlimmsten Fall irreparabel, bleibt neben einer neuen Erfahrung auch das gute Gefühl, es wenigstens versucht zu haben. Welche Gegenstände repariert werden können ist von Ort zu Ort unterschiedlich und erstreckt sich von Haushaltsgeräten über Spielzeug und Unterhaltungselektronik bis zu Textilien, Fahrrädern und Möbeln. Die Treffen finden meist dort statt, wo Räumlichkeiten kostenlos zur Verfügung stehen, wie Bibliotheken, Schulen oder Vereinsräume. Die Veranstaltungen sind nicht-kommerziell organisiert und weitestgehend kostenlos. Nur für Ersatzteile muss gezahlt werden. Jedoch werden Spenden sehr geschätzt mit denen neue Werkzeuge, Verbrauchsmaterialien oder andere Dinge wie Flyer und T-Shirts finanziert werden. Ein Repair Café ist kein "kostenloser Reparatur-Service", das in Konkurrenz mit Reparatur-Profis steht. Die Gäste beim Repair Café zählen meist nicht zu deren Kunden, da eine Reparatur teilweise teurer als ein Neukauf ist. Ein Repair Café zeigt, dass es trotzdem eine Alternative zum Wegwerfen gibt und so Müll vermeidet, Ressourcen spart und die Umwelt schont. Insbesondere jüngere Menschen wissen oft nicht wie sich Dinge reparieren lassen. Dabei können, dank einer guten Anleitung und fachkundiger Hilfe, selbst Laien Reparaturen an empfindlichen Smartphones eigenhändig bewerkstelligen. Anleitungen gibt es viele - etwa bei iFixit. Reparieren macht Spaß, vor allem, für jene, die Interesse an Technik, Selbermachen und Werken haben. Es ist relativ einfach und lehrt Gegenstände auf eine andere Weise wertzuschätzen. Wie viele Repair Cafés es insgesamt gibt lässt sich nicht einfach feststellen. Bei dem Netzwerk Reparatur-Initiativen sind ca. 900 Initiativen in Deutschland verzeichnet, die Stichting Repair Café kennt über 2.200 Repair Cafés weltweit.

**Quellen**

- vgl. Stichting Repair Café - Über (2021)
- vgl. Stichting Repair Café - FAQ (2021)
- vgl. Netzwerk Reparatur-Initiativen - Über uns (2021)
- vgl. Stiftung Warentest - Repair Cafés: Handys unter Anleitung selbst reparieren – funktioniert das? (2016)
- vgl. Verbraucherzentrale - Repair-Cafés (2021)
- Netzwerk Reparatur-Initiativen - Orte (2021)
- Stichting Repair Café - Besuchen (2021)


## 1.2 Die Geschichte des Repair Café Gütersloh

Einer dieser Orte ist das Repair Café Gütersloh - selbst nur bei dem Netzwerk Reparatur-Initiativen gelistet. Das Repair Café Gütersloh wurde im März 2019 gestartet und findet üblicherweise jeden Monat in den Räumen der Stadtbibliothek Gütersloh statt. Im Zeitraum vom März 2019 bis Dezember 2021 gab es 22 Termine. Davon waren vier außerhalb der Reihe aufgrund einer aufwändigen Reparatur, der Nacht der Bibliotheken 2019 und den International Repair Days 2020 & 2021. Weiterhin wurden 15 Termine wegen Corona abgesagt.

Für die Termine wurde in unterschiedlicher Weise Werbung gemacht. Zuerst standen die Termine nur auf der Website des Makerspace Gütersloh und der Stadtbibliothek Gütersloh. Seit Januar 2020 werden die Termine als Pressemitteilung an die drei Zeitungen in Gütersloh - Glocke, Neue Westfälische & Westfalen-Blatt - gesendet. Ab dem International Repair Day im Oktober 2020 werden die Termine beim Radio Gütersloh genannt. Später kamen noch die beiden lokalen Zeitschriften gt!nfo und Gütsel dazu.

Vor Corona gab es keine Terminvergabe, sondern die Gäste konnten einfach vorbeikommen und warten. Seit Corona bzw. dem ersten Repair Café, welches wieder stattgefunden hat im August 2020 mussten sich die Gäste per E-Mail oder Telefon anmelden, damit es aufgrund von Wartezeiten nicht zu größeren Gruppen kommt. Nach dem International Repair Day 2020 wurde die telefonische Anmeldung auf einen Anrufbeantworter umgestellt, damit die Organisation von dem Repair Café entlastet wurde.

Die Daten zu den Reparaturen werden auf den sogenannten Laufzetteln erfasst. Für die ersten Termine bis einschließlich Juni 2019 wurde die Vorlage der Stichting Repair Café genutzt, welche von der Bielefelder Zukunftswerkstatt e.V. zur Verfügung gestellt wurde. Die Nutzung dieser Vorlage ist jedoch nicht ohne weiteres erlaubt. Die Materialien müssen bei Stichting Repair Café bestellt werden und es muss stets das offizielle Logo verwendet werden. Da beides nicht gegeben ist, wurde auf die Laufzettel ohne Personendaten vom Netzwerk Reparatur-Initiativen gewechselt, um nicht mehr Daten als notwendig zu erfassen. Da auf diesen keine explizite Einverständniserklärung zu der Hausordnung gegeben ist, wurde erst wieder auf die vorherigen gewechselt bis festgestellt wurde, dass es noch eine Variante mit Personendaten gibt. Fälschlicherweise ist das Unterschriftenfeld als Einverständniserklärung zur Hausordnung verstanden worden, dabei handelt es sich jedoch um die Zustimmung zur Speicherung der Personendaten. Für die Monate August bis Oktober 2020 sind die Laufzettel nicht mehr auffindbar, aber es gibt Informationen zu den Terminen auf dem Netzwerk Reparatur-Initiativen und im Makerspace Gütersloh Forum. Ab Oktober 2021 wurde auf Laufzettel gewechselt, die selbst erstellt wurden und die Zustimmung der Hausordnung indirekt abfragen.

Für die ersten Termine kann das Spendenaufkommen teilweise aus den Kassenberichten entnommen werden. Für manche Termine ist keine Spende verzeichnet, sodass anzunehmen ist, dass die späteren Spenden für alle vorhergehenden Termine zusammen gelten. Es wurde bei der Kassenprüfung im Jahr 2021 festgestellt, dass dies ordentlicher und explizit erfolgen sollte. Daher sind seit September 2021 die Beträge in der Statistik aufgeführt.

Damit das Repair Café in Gütersloh ein durchgehend gleiches bis steigendes Niveau hält, ist das Wissen zur allgemeinen Organisation der Termine, der Vorbereitung des eigentlichen Repair Café, inklusive einer Checkliste welches Werkzeug mitgenommen wird und zum Resümee des Repair Café im Forum des Makerspace Gütersloh dokumentiert und kann von allen Beteiligten verändert und erweitert werden.

**Tab. 1: Termine des Repair Café Gütersloh**

|     Datum      | Laufzettel-Typ                              | Werbung                                                                           | Terminvergabe              | Laufzettel |  Spenden | Kommentar                     |
| :------------: | ------------------------------------------- | --------------------------------------------------------------------------------- | -------------------------- | :--------: | -------: | ----------------------------- |
|   02.03.2019   | repaircafe.org                              | Website<br>Stadtbibliothek                                                        | keine                      |     16     |  73,80 € |                               |
|   15.03.2019   | repaircafe.org                              | Website<br>Stadtbibliothek                                                        | keine                      |     26     | 150,90 € | Nacht der Bibliotheken        |
|   06.04.2019   | repaircafe.org                              | Website<br>Stadtbibliothek                                                        | keine                      |     12     |  90,55 € |                               |
|   04.05.2019   | repaircafe.org                              | Website<br>Stadtbibliothek                                                        | keine                      |     13     |  43,61 € |                               |
|   29.05.2019   | repaircafe.org                              | Website<br>Stadtbibliothek                                                        | keine                      |     1      |          | Sondertermin mit Einzelperson |
|   01.06.2019   | repaircafe.org                              | Website<br>Stadtbibliothek                                                        | keine                      |     11     |  70,00 € |                               |
|   06.07.2019   | repaircafe.org                              | Website<br>Stadtbibliothek                                                        | keine                      |     9      | 110,70 € |                               |
|   03.08.2019   | reparatur-initiativen.de ohne Personendaten | Website<br>Stadtbibliothek                                                        | keine                      |     9      |  82,02 € |                               |
|   07.09.2019   | reparatur-initiativen.de ohne Personendaten | Website<br>Stadtbibliothek                                                        | keine                      |     11     |          |                               |
|   05.10.2019   | reparatur-initiativen.de ohne Personendaten | Website<br>Stadtbibliothek                                                        | keine                      |     11     |  14,00 € |                               |
|   02.11.2019   | repaircafe.org                              | Website<br>Stadtbibliothek                                                        | keine                      |     11     |  31,00 € |                               |
|   07.12.2019   | repaircafe.org                              | Website<br>Stadtbibliothek                                                        | keine                      |     16     |  54,40 € |                               |
|   11.01.2020   | reparatur-initiativen.de                    | Website<br>Stadtbibliothek<br>Zeitung                                             | keine                      |     25     |          | Verschoben wegen Neujahr      |
|   01.02.2020   | reparatur-initiativen.de                    | Website<br>Stadtbibliothek<br>Zeitung                                             | keine                      |     17     |          |                               |
|   07.03.2020   | reparatur-initiativen.de                    | Website<br>Stadtbibliothek<br>Zeitung                                             | keine                      |     23     | 185,30 € |                               |
| ~~04.04.2020~~ |                                             |                                                                                   |                            |            |          | Absage wegen Corona           |
| ~~02.05.2020~~ |                                             |                                                                                   |                            |            |          | Absage wegen Corona           |
| ~~06.06.2020~~ |                                             |                                                                                   |                            |            |          | Absage wegen Corona           |
| ~~04.07.2020~~ |                                             |                                                                                   |                            |            |          | Absage wegen Corona           |
|   01.08.2020   | reparatur-initiativen.de online             | Website<br>Stadtbibliothek<br>Zeitung                                             | E-Mail<br>Telefon          |     8      |          |                               |
|   05.09.2020   | makerspace-gt Forum                         | Website<br>Stadtbibliothek<br>Zeitung                                             | E-Mail<br>Telefon          |     2      |  73,00 € |                               |
| ~~03.10.2020~~ |                                             |                                                                                   |                            |            |          | Absage wegen Corona           |
|   17.10.2020   | reparatur-initiativen.de online             | Website<br>Stadtbibliothek<br>Zeitung<br>Radio                                    | E-Mail<br>Telefon          |     14     |  82,20 € | International Repair Day      |
| ~~07.11.2020~~ |                                             |                                                                                   |                            |            |          | Absage wegen Corona           |
| ~~05.12.2020~~ |                                             |                                                                                   |                            |            |          | Absage wegen Corona           |
| ~~02.01.2021~~ |                                             |                                                                                   |                            |            |          | Absage wegen Corona           |
| ~~06.02.2021~~ |                                             |                                                                                   |                            |            |          | Absage wegen Corona           |
| ~~06.03.2021~~ |                                             |                                                                                   |                            |            |          | Absage wegen Corona           |
| ~~03.04.2021~~ |                                             |                                                                                   |                            |            |          | Absage wegen Corona           |
| ~~01.05.2021~~ |                                             |                                                                                   |                            |            |          | Absage wegen Corona           |
| ~~05.06.2021~~ |                                             |                                                                                   |                            |            |          | Absage wegen Corona           |
| ~~03.07.2021~~ |                                             |                                                                                   |                            |            |          | Absage wegen Corona           |
| ~~07.08.2021~~ |                                             |                                                                                   |                            |            |          | Absage wegen Corona           |
|   04.09.2021   | makerspace-gt                               | Website<br>Stadtbibliothek<br>Zeitung<br>Radio<br>gt-info<br>Gütsel<br>Klimawoche | E-Mail<br>Anrufbeantworter |     7      |  32,00 € | Gütersloher Klimawoche        |
|   02.10.2021   | makerspace-gt                               | Website<br>Stadtbibliothek<br>Zeitung<br>Radio<br>gt-info<br>Gütsel               | E-Mail<br>Anrufbeantworter |     11     | 138,00 € |                               |
|   16.10.2021   | makerspace-gt                               | Website<br>Stadtbibliothek<br>Zeitung<br>Radio<br>gt-info<br>Gütsel               | E-Mail<br>Anrufbeantworter |     8      |  55,02 € | International Repair Day      |
|   06.11.2021   | makerspace-gt                               | Website<br>Stadtbibliothek<br>Zeitung<br>Radio<br>gt-info<br>Gütsel               | E-Mail<br>Anrufbeantworter |     14     |  87,90 € |                               |
| ~~04.12.2021~~ |                                             |                                                                                   |                            |            |          | Absage wegen Corona           |

Quelle: eigene Darstellung

**Quellen**

- Netzwerk Reparatur-Initiativen - Makerspace Gütersloh (2022)
- Makerspace Gütersloh Forum - International Repair Day (2020)
- Makerspace Gütersloh - Termine (2022)
- Stadtbibliothek Gütersloh - Veranstaltungen (2022)
- GitHub Makerspace Gütersloh - repair-cafe/Öffentlichkeitsarbeit (2020)
- Die Glocke (2022)
- Neue Westfälische (2022)
- Westfalen-Blatt (2022)
- Radio Gütersloh - Veranstaltungstipps für den Kreis Gütersloh (2022)
- gt!nfo - Termine (2022)
- Gütsel Online - Veranstaltungen (2022)
- Makerspace Gütersloh Forum - Repair Café & Corona (2020)
- GitHub Makerspace Gütersloh - repair-cafe/Anrufbeantworter (2020)
- Bielefelder Zukunftswerkstatt - Repair Café (2022)
- GitHub Makerspace Gütersloh - repair-cafe/repaircafe.org (2019)
- Stichting Repair Café - Selbst starten (2021)
- GitHub Makerspace Gütersloh - repair-cafe/reparatur-initiativen.de (2019)
- Makerspace Gütersloh Forum - Laufzettel (2019)
- GitHub Makerspace Gütersloh - repair-cafe/Laufzettel (2020)
- Makerspace Gütersloh Forum - Repair Café Statistik (2019)
- GitHub Makerspace Gütersloh - protokolle/Kassenbericht Geschäftsjahr (2019)
- GitHub Makerspace Gütersloh - protokolle/Mitgliederversammlung (2021)
- Makerspace Gütersloh Forum - Organisation (2021)
- Makerspace Gütersloh Forum - Checkliste Repair Café (2020)
- Makerspace Gütersloh Forum - Resümee Repair Café (2019)


# 2. Kapitel 2 - R und der Ablauf einer Datenanalyse

In diesem Kapitel gibt es eine kurze Vorstellung der Statistik-Software R, warum diese im Kontext des Makerspace zur Auswertung der Daten von dem Repair Café verwendet wird und in welchen Schritten eine Datenanalyse abläuft.

Für eine erste Auswertung der Daten in dieser Hausarbeit wird die freie Programmiersprache R verwendet. Im Gegensatz zu Excel und SPSS bietet R verschiedene Vorteile:

- Es handelt sich um freie Software die kostenlos genutzt werden kann.
- Es gibt eine klare Trennung von Daten und Analysen.
- R ist skriptfokussiert, wodurch die Analysten nachvollziehbar, wiederverwendbar und versionierbar sind.
- Es gibt eine aktive Community, über die viele verschiedene Pakete bereitgestellt werden.
- Es gibt viele Schnittstellen zum Import und Export von Daten und Auswertungen.

Weiterhin ist es im Kontext des Makerspace Gütersloh besonders geeignet, da es schon Personen gibt, die sich mit R auskennen und der Umstand, dass es sich um freie Software handelt, gut mit der Maker-Szene vereinbar ist.

Nach Wickham & Grolemund besteht eine vollständige Datenanalyse aus sechs Schritten, siehe Abb. 2.

**Abb. 2: Schritte einer Datenanalyse**  
![](Abbildungsverzeichnis/data-science.png)  
Quelle: Wickham, Hadley/Grolemund, Garrett (2017) Introduction

Im ersten Schritt "Import" werden die Daten in einer maschinenlesbaren Form in die Statistik-Software geladen. Dabei werden verschiedene Formate unterstützt. 
Unmittelbar nach dem "Import" folgt der Schritt "Tidy" in dem die Daten bereinigt werden, um einen in sich stimmigen Datensatz zu erhalten. Beispielsweise werden unterschiedliche Schreibweisen und Abkürzungen oder Synonyme vereinheitlicht. 
Mit diesen Aufgeräumten Daten kann nun in dem Block "Understand" die eigentliche Datenanalyse stattfinden. Dabei werden die drei Schritte "Transform", "Visualise" und "Model" sich immer wieder gegenseitig beeinflussen, sodass es keinen starren Ablauf gibt. 
Im Schritt "Transform" werden die Daten in geeigneter Form aufbereitet. Das beinhaltet das Aussortieren von Datensätzen, die Zusammenfassung von Daten oder das Ableiten weiterer Daten. 
Im Schritt "Visualise" werden die Daten grafisch dargestellt und können so getreu dem Motto "Ein Bild sagt mehr als 1.000 Worte" von Menschen interpretiert werden. Bei einer guten Visualisierung werden dabei unerwartete Zusammenhänge sichtbar, die neue Fragen aufwerfen. Eine falsch gewählte Skalierung oder ein ungeeigneter Diagrammtyp kann jedoch auch zu einer Fehlinterpretation führen. 
Parallel zu dem Schritt "Visualise" gibt es den Schritt "Model", bei dem Modelle erstellt werden, um die Daten zu erklären. Dafür bedarf es keiner Interpretation durch einen Menschen, sondern die Hypothesen werden mathematisch überprüft. Unerwartete Zusammenhänge oder neue Fragen ergeben sich daraus nicht. 
Nach dem Verstehen und Analysieren der Daten folgt der letzte Schritt "Communicate". Dabei werden jedoch nicht die Daten, sondern die Erkenntnisse vermittelt und das in einer für die Zielgruppe verständlichen Form.

**Quellen**

- vgl. Ottmann, Sebastian (2016) S.71f
- vgl. Sauer, Sebastian (2019) S. 3, 16f
- vgl. R Project - Import (2022)
- vgl. Rassow, Uta (2019) S. 3
- vgl. Wickham, Hadley/Grolemund, Garrett (2017) Introduction
- vgl. Prange, Michael (2021) S. 1


# 3. Kapitel 3 - Datenaufbereitung

In diesem Kapitel werden die Daten der Laufzettel digitalisiert, importiert und bereinigt, um dann erste einfache Auswertungen durchzuführen. Die Skripte zur Auswertung und die Daten in Form von CSV Dateien sind im Anhang zu finden.


## 3.1 Import

Die Daten liegen in unterschiedlichen Formaten vor. Über die Zeit wurden verschiedene Typen der Laufzettel genutzt. Bei einigen Terminen waren keine Laufzettel mehr vorhanden. Um die Daten trotzdem auszuwerten, wurde auf die digitalen Laufzettel von reparatur-initiativen.de oder die Einträge aus dem Makerspace Gütersloh Forum zurückgegriffen.

**Abb. 3: Laufzettel - repaircafe.org**  
![](Abbildungsverzeichnis/Laufzettel%20-%20repaircafe.org.png)  
Quelle: GitHub Makerspace Gütersloh - repair-cafe/repaircafe.org (2019)

**Abb. 4: Laufzettel - reparatur-initiativen.de**  
![](Abbildungsverzeichnis/Laufzettel%20-%20reparatur-initiativen.de.png)  
Quelle: GitHub Makerspace Gütersloh - repair-cafe/reparatur-initiativen.de (2019)

**Abb. 5: Laufzettel - reparatur-initiativen.de ohne Personendaten**  
![](Abbildungsverzeichnis/Laufzettel%20-%20reparatur-initiativen.de%20ohne%20Personendaten.png)  
Quelle: GitHub Makerspace Gütersloh - repair-cafe/reparatur-initiativen.de (2019)

**Abb. 6: Laufzettel - makerspace-gt**  
![](Abbildungsverzeichnis/Laufzettel%20-%20makerspace-gt.png)  
Quelle: GitHub Makerspace Gütersloh - repair-cafe/Laufzettel (2020)

**Abb. 7: Laufzettel - makerspace-gt Forum**  
![](Abbildungsverzeichnis/Laufzettel%20-%20makerspace-gt%20Forum.png)  
Quelle: Makerspace Gütersloh Forum - Repair Café Statistik (2019)

**Abb. 8: Laufzettel - reparatur-initiativen.de online**  
![](Abbildungsverzeichnis/Laufzettel%20-%20reparatur-initiativen.de%20online.png)  
Quelle: Netzwerk Reparatur-Initiativen - Makerspace Gütersloh (2022)

Für eine Datenanalyse müssen die Daten als erstes in der Statistik-Software verfügbar gemacht werden. Die Digitalisierung der Laufzettel ist durch manuelles Abtippen erfolgt. Für jede Art von Laufzettel wurde eine eigene CSV Datei angelegt. Es gab 266 Laufzettel mit 40 verschiedenen Datenfeldern. Durch eine Vereinheitlichung von Feldbezeichnungen konnte die Anzahl an Datenfeldern auf 28 reduziert werden.

Beim Abtippen der Daten sind einige Dinge aufgefallen:

- Es sind Kontaktmöglichkeiten angegeben, die nicht abgefragt wurden.
- Laufzettelnummern werden nicht konsequent ausgefüllt.
- Es sind mehrere Geräte auf einem Laufzettel erfasst.
- Es ist nicht erkennbar ob vertagte Termine wieder aufgenommen wurden.
- Es ist nicht vorgesehen, dass ein Gerät/Gegenstand von mehreren Personen mit unterschiedlichen Altern repariert wurde.
- Bei den makerspace-gt Laufzetteln gibt es kein separates Feld für das Gerät.
- Bei den Laufzetteln von Reparaturinitiativen ist keine Beschreibung zur Reparatur vorgesehen.
- Es gibt Unterschriften, die nicht erforderlich sind oder fehlende Unterschriften.

Weiterhin gab es noch Auffälligkeiten bezüglich der Gestaltung der Laufzettel:

- Bei dem repaircafe.org Laufzettel ist sehr wenig Platz für Gerätebezeichnung.
- Auf den makerspace-gt Laufzetteln ist bei den Feldern Fehlerbeschreibung und Beschreibung zu wenig Platz.

**Quellen**

- GitHub Makerspace Gütersloh - repair-cafe/repaircafe.org (2019)
- GitHub Makerspace Gütersloh - repair-cafe/reparatur-initiativen.de (2019)
- GitHub Makerspace Gütersloh - repair-cafe/Laufzettel (2020)
- Netzwerk Reparatur-Initiativen - Makerspace Gütersloh (2022)
- Makerspace Gütersloh Forum - Repair Café Statistik (2019)


## 3.2 Tidy

Der zweite Schritt einer Datenanalyse ist die Bereinigung der Daten. Ebenso wie der Import erfolgt dieser Schritt manuell. 
Wenn auf einem Laufzettel mehrere Gegenstände aufgeschrieben sind, wurde die Zeile kopiert. 
Anschließend wurden die einzelnen Datenfelder qualitativ bewertet und so weit wie möglich kategorisiert. Werte, die in falschen Datenfeldern standen wurden verschoben. Bei rätselhaften Daten wurden die Personen befragt, welche die Reparatur durchgeführt haben - in vielen Fällen konnten diese sich noch an die Geräte erinnern. 
Insgesamt ist der Datensatz mit Vorsicht zu genießen, da er nur nach bestem Wissen und Gewissen bereinigt werden konnte.

Logische Datenfelder wie "Einverständnis Hausordnung", "Fehler gefunden" oder "Name" erhielten die Werte ```TRUE```, ```FALSE```, ```NA``` oder ```NULL```, wobei ```NA``` keine Angabe und ```NULL``` nicht abgefragt bedeutet. 
Um die Werte von der "Reparaturkategorie", "Gerät/Gegenstand", "Stromversorgung" und "Marke" zu vereinheitlichen wurde auf die Datenbank des reparatur-initiativen.de online Formular zurückgegriffen. Damit die eigentliche Information über das Gerät erhalten blieb wurde das Datenfeld "Gerät/Gegenstand-raw" als Kopie angelegt. 
Auch bei dem Defekt wurden die Daten zunächst in ein neues Datenfeld "Defekt-raw" kopiert. Für die Kategorisierung gibt es jedoch keine bestehende Datenbank, sodass die möglichen Werte selbst iterativ bestimmt wurden. 
Das Feld "Repariert von" wurde in die Felder "Repariert von mir" und "Repariert mit Hilfe" aufgeteilt. Die Bezeichnung ist aus Sicht der Person zu lesen die das Repair Café mit einem Gegenstand besucht hat. 
Die Felder "Ergebnis" der Reparatur, der "Beschreibung" und "Fehler gefunden" mussten qualitativ interpretiert werden. Um eine gute Kategorisierung für die weitere Datenanalyse zu ermöglichen, wurde noch ein weiteres Datenfeld "Ergebnis-Detail" eingefügt. 
Für die Werbung konnte eine eigene Auswahl erstellt werden, da hier mehrere Werte möglich sind werden diese durch einen Zeilenumbruch ```\n``` getrennt. 
Bei dem Feld "Alter" wurde direkt eine Zuordnung zu der "Alterskategorie" durchgeführt.

Im Anhang "Werte.csv" sind die möglichen Werte bzw. der erwartete Datentyp (```DATE```, ```INT```, ```STRING```) pro Datenfeld aufgeschrieben.

**Tab. 2: Datenfelder - raw/tidy**

| variable                       | length-raw | length | unique-raw | unique |
| ------------------------------ | ---------: | -----: | ---------: | -----: |
| Laufzettel                     |        266 |    281 |          6 |      6 |
| Datum                          |        266 |    281 |         22 |     22 |
| Nummer                         |        258 |    273 |         51 |     51 |
| Einverständnis Hausordnung     |        100 |    147 |          1 |      1 |
| Reparaturkategorie             |        139 |    269 |         16 |      9 |
| Gerät/Gegenstand               |        175 |    274 |        131 |    104 |
| Gerät/Gegenstand-raw           |         NA |    242 |         NA |    172 |
| Stromversorgung                |         39 |    232 |          7 |      5 |
| Marke                          |          5 |     42 |          5 |     31 |
| Baujahr                        |         10 |     10 |          9 |      9 |
| Defekt                         |        264 |    273 |        250 |     22 |
| Defekt-raw                     |         NA |    273 |         NA |    243 |
| repariert von                  |        211 |     NA |         33 |     NA |
| repariert von mir              |         NA |      8 |         NA |      1 |
| repariert mit Hilfe            |         NA |    216 |         NA |      1 |
| Fehler gefunden                |         63 |     68 |          5 |      1 |
| Ergebnis                       |        196 |    247 |         38 |      5 |
| Ergebnis Detail                |         NA |     69 |         NA |     11 |
| Beschreibung                   |        131 |    125 |        125 |    123 |
| Benutzungsverbot               |          9 |     10 |          1 |      1 |
| Unterschrift Benutzungsverbot  |         57 |     63 |          1 |      1 |
| Werbung durch                  |         38 |     43 |         21 |     13 |
| Feedback                       |          4 |      6 |          3 |      3 |
| Name                           |        163 |    170 |          1 |      1 |
| Alter                          |         92 |    115 |         50 |     51 |
| Alterskategorie                |        107 |    214 |         11 |      5 |
| Geschlecht                     |        162 |    171 |          3 |      2 |
| E-Mail                         |         55 |     56 |          2 |      1 |
| Telefon                        |         51 |     55 |          1 |      1 |
| Adresse                        |         40 |     44 |          1 |      1 |
| Newsletter Repair Café         |         15 |     16 |          1 |      1 |
| Newsletter Verein              |          9 |      9 |          1 |      1 |
| Unterschrift Datenverarbeitung |         40 |     42 |          1 |      1 |

Quelle: eigene Darstellung

In Tabelle 2 ist eine Gegenüberstellung der Daten vor und nach der Bereinigung. Es sind 15 Geräte mehr im Datensatz vorhanden, als es Laufzettel gab. 
Die Bezeichnung der Gegenstände konnte von für gut 100 Geräte mehr vergeben werden, da diese in anderen Datenfeldern benannt wurden, und trotzdem sind es nun "nur noch" 104 verschiedene anstelle von 131. Auch die Reparaturkategorie konnte daraus für wesentlich mehr Geräte abgeleitet und vereinheitlicht werden. 
Besonders stark hat sich die Anzahl unterschiedlicher Fehler reduziert von 250 auf 22.

**Quellen**

- GitHub anstiftung - mapped-repair-events (2021)


## 3.3 Transform

Der dritte Schritt bei einer Datenanalyse und der erste Schritt des Verstehens der Daten ist "Transform". Hierbei werden neue Informationen aus den vorhandenen Daten abgeleitet. 
Bei den Werten für "Reparaturkategorie", "Repariert von mir", "Repariert mit Hilfe", "Ergebnis", "Ergebnis Detail" und "Alterskategorie" ist dies bereits im vorherigen Schritt bei der Bereinigung der Daten erfolgt.

Auf den Laufzetteln sind an verschiedenen Stellen Unterschriften vorgesehen, um das Einverständnis mit der Hausordnung, die Haftung, die Datenverarbeitung und den Ausschluss einer Benutzung zu klären. Bei der Digitalisierung der Laufzettel ist aufgefallen, dass diese nicht konsequent ausgefüllt sind. Ebenso fehlt oft eine Kontaktmöglichkeit, wenn die Reparatur vertagt wurde, um zu klären, ob das Hilfsmittel oder Ersatzteil besorgt werden konnte. In Tabelle 3 ist zu sehen, dass es in jedem formellen Punkt eine Differenz zwischen den Soll- und Ist-Anzahlen gibt und auch öfter Werte gesammelt werden, die nicht benötigt werden. Insgesamt gibt es in 203 Fällen eine rechtliche Unsicherheit, in 64 Fällen eine unnötige Unterschrift, in 21 Fällen ist keine Kontaktmöglichkeit bei vertagter Reparatur gegeben und in 71 Fällen wurden Personenbezogene Daten erhoben, die nicht erforderlich waren.

**Tab. 3: Formalien**

| Formalie                       | soll |  ist | Differenz | unnötig |
| ------------------------------ | ---: | ---: | --------: | ------: |
| Zustimmung Hausordnung         |  161 |  147 |        14 |      NA |
| Haftung geklärt                |  257 |  223 |        34 |      NA |
| Unterschrift Datenverarbeitung |  186 |   32 |       154 |      10 |
| Unterschrift Benutzungsverbot  |   10 |    9 |         1 |      54 |
| Summe                          |   NA |   NA |       203 |      64 |
| Kontakt bei vertagt            |   32 |   11 |        21 |      71 |

Quelle: eigene Darstellung


# 4. Kapitel 4 - Beantwortung der Eingangsfragen

Mit den ersten drei Schritten der Datenanalyse werden in diesem Kapitel die Eingangsfragen beantwortet.


## 4.1 Wie lassen sich die vorhandenen Daten aufbereiten?

Die Aufbereitung der Daten erforderte viel Handarbeit. Die Daten wurden digitalisiert, durch Interviews und Interpretationen erweitert und Kategorisiert. Für die Kategorisierung konnte teilweise auf Vorgaben zurückgegriffen werden, in anderen Fällen wurden die möglichen Werte selbst bestimmt.


## 4.2 Gibt es unnütze Daten oder fehlen Daten?

Aus den 28 Datenfeldern direkt nach dem Import sind durch die Kategorisierung 33 Datenfelder geworden. Dies beruht darauf, dass die Angaben aufgeteilt wurden, um sich besser kategorisieren zu lassen. Unabhängig von den erhobenen Daten sollte darauf geachtet werden, dass die Laufzettel vollständig und korrekt ausgefüllt werden. 
Eine Adresse der Gäste ist in keinem Fall erforderlich. Weiterhin reicht es, wenn eine Kontaktmöglichkeit (E-Mail oder Telefon) angegeben ist, sofern ein erneuter Termin notwendig ist oder die Eintragung in einen Newsletter erfolgen soll. Ob die Abfrage des Geschlechts in einer sich diversifizierenden Welt noch angemessen ist, kann nicht in dieser Hausarbeit beantwortet werden. Wenn, dann sollten jedoch mehr als zwei Möglichkeiten vorgesehen werden. 
Es wird nicht erfasst, ob eine Reparatur wieder aufgenommen wurde, ein Gerät/Gegenstand selbst repariert wurde und wie lange die Reparatur gedauert hat.

Mit diesen Erkenntnissen ergibt sich ein Vorschlag an Abfragen für einen Laufzettel:

- Kopfdaten vor Reparaturbeginn
  - Datum
  - Nummer
  - Einverständnis Hausordnung
- Beschreibung von Gerät/Gegenstand
  - Kategorie (Reparaturkategorie)
  - Gerät/Gegenstand
  - Marke
  - Bezeichnung (Gerät/Gegenstand-raw)
  - Baujahr
  - Stromversorgung
  - Defekt
  - Erläuterung (Defekt-raw)
- Wer hat Repariert?
  - repariert von mir
  - repariert mit Hilfe
- Wie ist die Reparatur ausgegangen?
  - Fehler gefunden
  - Ergebnis
  - Ergebnis Detail
  - Beschreibung
  - Benutzungsverbot
  - Unterschrift Benutzungsverbot
  - Dauer bzw. Beginn und Ende der Reparatur
- Feedback
- Daten für die Statistik
  - Werbung durch
  - Alter
- Wenn ein weiterer Kontakt erwünscht ist
  - Name
  - Kontakt (E-Mail oder Telefon)
  - Newsletter Repair Café
  - Newsletter Verein
  - Unterschrift Datenverarbeitung


## 4.3 Welche Informationen lassen sich daraus ableiten?

In Kapitel 3.3 konnte bereits gezeigt werden, dass die Laufzettel nicht korrekt ausgefüllt werden. 
Durch die Kategorisierung der Daten kann untersucht werden welche Geräte und Gegenstände zu einer Reparatur angeboten werden, wie die Erfolgsquote dafür ist und wie sich der Erfolg über die Zeit verändert. 
Die Daten "Werbung durch", Alter und Anzahl an Reparaturen pro Termin lassen sich daraufhin untersuchen, ob eine Werbung einen Einfluss hat und welche Altersgruppen dadurch angesprochen werden. 
Da die Anzahl an unterschiedlichen Fehlern stark eingegrenzt werden konnte lassen sich dafür ggf. Reparaturanleitungen erstellen, damit eine Reparatur selbst durchgeführt werden kann. 


# 5. Kapitel 5 - Fazit

Die Arbeit mit einer Statistik-Software wie R benötigt Einarbeitungszeit und einen strukturierten Datensatz. 
Um diesen Datensatz zu bekommen, sollten die Daten nicht direkt eingegeben werden, sondern durch eine Qualitätskontrolle gehen. Bei der aktuellen Organisation der Reparaturtermine in Gütersloh bietet es sich an, dass der Empfang diese Rolle übernimmt. 
Wenn die Digitalisierung zeitnah oder während des Termins erfolgt, können fehlende Informationen noch leicht erfragt werden, sodass weniger Interpretation erforderlich ist.
Werden die Daten weiterhin auf Papier erfasst ergeben sich dadurch mehrere Vorteile. Die Qualitätskontrolle erfolgt vor der Digitalisierung, es wird nur ein Gerät zur Eingabe der Daten benötigt, die personenbezogenen Daten liegen nur auf Papier vor und können höchstens einmal verloren gehen.

Das Netzwerk Reparatur-Initiativen fordert dazu auf die Reparaturen online einzutragen. 
Für die einzelnen Initiativen selbst bietet dies bereits eine einfache Visualisierung der Daten. 
Über alle Initiativen hinweg ergeben sich belastbare Zahlen für erfolgreiche und nicht mögliche Reparaturen. 
Diese bilden dann die Gegenstimme zur Industrie bezügliche einem Recht auf Reparatur und werden daraufhin analysiert welche Fehler bei Geräten immer wieder auftauchen. 
Das Netzwerk Reparatur-Initiativen sagt selbst, dass die Gestaltung der digitalen Laufzettel noch nicht abgeschlossen ist, sodass die Erkenntnisse dieser Hausarbeit bezüglich Kategorisierung der Fehler und weiterer Datenfelder an die Verantwortlichen kommuniziert und mit ihnen diskutiert werden sollte.

Ein anders strukturierter Laufzetteln kann nicht nur dabei helfen die Daten besser zu erfassen, sondern auch als Wegweiser durch eine Reparatur dienen. 
Wenn häufige Fehlerursachen und Anleitungen zur Behebung dieser Fehler bekannt und dokumentiert sind kann dies dazu führen, dass mehr Geräte selbst repariert werden. Wenn bei einer selbst durchgeführten Reparatur Schönheitsfehler auftreten werden diese, aufgrund des IKEA-Effektes, eher toleriert und das Gerät bleibt länger in Benutzung.

Da einige Gäste durch eine Empfehlung zu dem Repair Café gekommen sind, wäre es sinnvoll eine niederschwellige Möglichkeit zur Inkenntnissetzung von Bekannten einzurichten. Etwa durch einen Postkartenvordruck, der nur durch eine Nachricht und die Anschrift ergänzt werden muss oder der Möglichkeit ein Foto von sich, dem Gerät und den Informationen zum nächsten Termin zu erstellen welches dann als Nachricht geteilt werden kann. 
Bei den allgemeinen Werbetexten im Internet und der Zeitung kann die Erfolgsquote mit angegeben werden, um zu zeigen wie wahrscheinlich eine Reparatur ist.

Die Betrachtung von Reparaturdaten führt zu vielen Interessanten Fragen, die darauf warten beantwortet zu werden.

**Quellen**

- vgl. Netzwerk Reparatur-Initiativen - Auf dem Weg zur Reparatur-Statistik (2020)
- vgl. Wikipedia - IKEA-Effekt (2011)


# Anhang

- **Anhang I: Laufzettel - repaircafe.org-raw.csv**
- **Anhang II: Laufzettel - reparatur-initiativen.de-raw.csv**
- **Anhang III: Laufzettel - reparatur-initiativen.de ohne Personendaten-raw.csv**
- **Anhang IV: Laufzettel - makerspace-gt-raw.csv**
- **Anhang V: Laufzettel - reparatur-initiativen.de online-raw.csv**
- **Anhang VI: Laufzettel - makerspace-gt Forum-raw.csv**
- **Anhang VII: 1 import.R**
- **Anhang VIII: Laufzettel-raw.csv**
- **Anhang IX: Datenfelder-raw.csv**
- **Anhang X: Werte.csv**
- **Anhang XI: Laufzettel-tidy.csv**
- **Anhang XII: 2 tidy.R**
- **Anhang XIII: Laufzettel-tidy.csv**
- **Anhang XIV: Datenfelder-raw-tidy.csv**
- **Anhang XV: 3 transform.R**
- **Anhang XVI: Formalien.csv**


# Literatur- und Quellenverzeichnis

|                                                                                                        |                                                                                                                                                                                                                              |
| ------------------------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Bielefelder Zukunftswerkstatt - Repair Café (2022)                                                     | Bielefelder Zukunftswerkstatt - Repair Café, https://bielefelder-zukunftswerkstatt.de/Repair-Cafe/ (Zugriff: 02.01.2022)                                                                                                     |
| Die Glocke (2022)                                                                                      | Die Glocke, https://www.die-glocke.de/ (Zugriff: 02.01.2022)                                                                                                                                                                 |
| GitHub anstiftung - mapped-repair-events (2021)                                                        | GitHub anstiftung - mapped-repair-events, https://github.com/anstiftung/mapped-repair-events/blob/main/config/sql/database.sql (Zugriff: 18.02.2022)                                                                         |
| GitHub Makerspace Gütersloh - protokolle/Kassenbericht Geschäftsjahr (2019)                            | GitHub Makerspace Gütersloh - protokolle/Kassenbericht Geschäftsjahr, https://github.com/makerspace-gt/protokolle/blob/main/2020/2020-02-28_Kassenbericht_Gesch%C3%A4ftsjahr_2019.pdf (Zugriff: 02.01.2022)                  |
| GitHub Makerspace Gütersloh - protokolle/Mitgliederversammlung (2021)                                  | GitHub Makerspace Gütersloh - protokolle/Mitgliederversammlung, S. 11, https://github.com/makerspace-gt/protokolle/blob/main/2021/2021-02-27%20Mitgliederversammlung%20-%20Pr%C3%A4sentation.pdf (Zugriff: 02.01.2022)       |
| GitHub Makerspace Gütersloh - repair-cafe/Anrufbeantworter (2020)                                      | GitHub Makerspace Gütersloh - repair-cafe/Anrufbeantworter, https://github.com/makerspace-gt/repair-cafe/tree/master/Anrufbeantworter (Zugriff: 02.01.2022)                                                                  |
| GitHub Makerspace Gütersloh - repair-cafe/Laufzettel (2020)                                            | GitHub Makerspace Gütersloh - repair-cafe/Laufzettel, https://github.com/makerspace-gt/repair-cafe/blob/master/Laufzettel/RCGT_Laufzettel_A6_3.pdf (Zugriff: 02.01.2022)                                                     |
| GitHub Makerspace Gütersloh - repair-cafe/Öffentlichkeitsarbeit (2020)                                 | GitHub Makerspace Gütersloh - repair-cafe/Öffentlichkeitsarbeit, https://github.com/makerspace-gt/repair-cafe/tree/master/%C3%96ffentlichkeitsarbeit (Zugriff: 30.12.2021)                                                   |
| GitHub Makerspace Gütersloh - repair-cafe/repaircafe.org (2019)                                        | GitHub Makerspace Gütersloh - repair-cafe/repaircafe.org, https://github.com/makerspace-gt/repair-cafe/tree/repaircafe.org (Zugriff: 02.01.2022)                                                                             |
| GitHub Makerspace Gütersloh - repair-cafe/reparatur-initiativen.de (2019)                              | GitHub Makerspace Gütersloh - repair-cafe/reparatur-initiativen.de, https://github.com/makerspace-gt/repair-cafe/tree/reparatur-initiativen.de (Zugriff: 02.01.2022)                                                         |
| gt!nfo - Termine (2022)                                                                                | gt!nfo - Termine, https://gt-info.de/termine (Zugriff: 02.01.2022)                                                                                                                                                           |
| Gütsel Online - Veranstaltungen (2022)                                                                 | Gütsel Online - Veranstaltungen, https://www.guetsel.de/veranstaltungen.html (Zugriff: 02.01.2022)                                                                                                                           |
| iFixit - Anleitungen (2021)                                                                            | iFixit - Anleitungen, https://de.ifixit.com/Anleitung (Zugriff: 31.12.2021)                                                                                                                                                  |
| iFixit - Reparaturmanifest (2020)                                                                      | iFixit - Reparaturmanifest, https://de.ifixit.com/Manifesto (Zugriff: 30.12.2021)                                                                                                                                            |
| Makerspace Gütersloh - Termine (2022)                                                                  | Makerspace Gütersloh - Termine, https://makerspace-gt.de/termine/ (Zugriff: 02.01.2022)                                                                                                                                      |
| Makerspace Gütersloh Forum - Checkliste Repair Café (2020)                                             | Makerspace Gütersloh Forum - Checkliste Repair Café, https://forum.makerspace-gt.de/t/checkliste-repair-cafe/278 (Zugriff: 02.01.2022)                                                                                       |
| Makerspace Gütersloh Forum - International Repair Day (2020)                                           | Makerspace Gütersloh Forum - International Repair Day, https://forum.makerspace-gt.de/t/international-repair-day/433 (Zugriff: 02.01.2022)                                                                                   |
| Makerspace Gütersloh Forum - Laufzettel (2019)                                                         | Makerspace Gütersloh Forum - Laufzettel, https://forum.makerspace-gt.de/t/laufzettel/426 (Zugriff: 02.01.2022)                                                                                                               |
| Makerspace Gütersloh Forum - Organisation (2021)                                                       | Makerspace Gütersloh Forum - Organisation, https://forum.makerspace-gt.de/t/oragnisation/561 (Zugriff: 02.01.2022)                                                                                                           |
| Makerspace Gütersloh Forum - Repair Café & Corona (2020)                                               | Makerspace Gütersloh Forum - Repair Café & Corona, https://forum.makerspace-gt.de/t/repair-cafe-corona/377 (Zugriff: 02.01.2022)                                                                                             |
| Makerspace Gütersloh Forum - Repair Café Statistik (2019)                                              | Makerspace Gütersloh Forum - Repair Café Statistik, https://forum.makerspace-gt.de/t/repair-cafe-statistik/47 (Zugriff: 02.01.2022)                                                                                          |
| Makerspace Gütersloh Forum - Resümee Repair Café (2019)                                                | Makerspace Gütersloh Forum - Resümee Repair Café, https://forum.makerspace-gt.de/t/resuemee-repair-cafe/215 (Zugriff: 02.01.2022)                                                                                            |
| Netzwerk Reparatur-Initiativen - Auf dem Weg zur Reparatur-Statistik (2020)                            | Netzwerk Reparatur-Initiativen - Auf dem Weg zur Reparatur-Statistik, https://www.reparatur-initiativen.de/post/auf-dem-weg-zur-reparatur-statistik (Zugriff: 22.02.2022)                                                    |
| Netzwerk Reparatur-Initiativen - Makerspace Gütersloh (2022)                                           | Netzwerk Reparatur-Initiativen - Makerspace Gütersloh, https://www.reparatur-initiativen.de/makerspace-gt (Zugriff: 02.01.2022)                                                                                              |
| Netzwerk Reparatur-Initiativen - Orte (2021)                                                           | Netzwerk Reparatur-Initiativen - Orte, https://www.reparatur-initiativen.de/orte (Zugriff: 30.12.2021)                                                                                                                       |
| Netzwerk Reparatur-Initiativen - Über uns (2021)                                                       | Netzwerk Reparatur-Initiativen - Über uns, https://www.reparatur-initiativen.de/seite/ueber-uns (Zugriff: 30.12.2021)                                                                                                        |
| Neue Westfälische (2022)                                                                               | Neue Westfälische, https://www.nw.de/ (Zugriff: 02.01.2022)                                                                                                                                                                  |
| Ottmann, Sebastian (2016)                                                                              | Skript Datenanalyse mit Excel, SPSS und R, Nürnberg 2016                                                                                                                                                                     |
| Prange, Michael (2021)                                                                                 | Stakeholder Analyse als Grundlage zur Definition von Projektzielen für das Projekt Kitchen Raid des Makerspace Gütersloh e.V., Bielefeld 2021                                                                                |
| R Project - Import (2022)                                                                              | R: Import, https://search.r-project.org/CRAN/refmans/rio/html/import.html (Zugriff: 13.02.2022)                                                                                                                              |
| Radio Gütersloh - Veranstaltungstipps für den Kreis Gütersloh (2022)                                   | Radio Gütersloh - Veranstaltungstipps für den Kreis Gütersloh, https://www.radioguetersloh.de/service/veranstaltungstipps.html (Zugriff: 02.01.2022)                                                                         |
| Rassow, Uta (2019)                                                                                     | Reparieren vs. Wegwerfen: Zusammenhang zwischen dem Alter der Besucher eines Repair Cafés und der Reparierbarkeit ihrer Geräte, Gütersloh 2019                                                                               |
| Sauer, Sebastian (2019)                                                                                | Moderne Datenanalyse mit R: Daten einlesen, aufbereiten, visualisieren, modellieren und kommunizieren, Wiesbaden 2019                                                                                                        |
| Stadtbibliothek Gütersloh - Veranstaltungen (2022)                                                     | Stadtbibliothek Gütersloh - Veranstaltungen, https://www.stadtbibliothek-guetersloh.de/veranstaltungen/ (Zugriff: 30.12.2021)                                                                                                |
| Stichting Repair Café - Besuchen (2021)                                                                | Stichting Repair Café - Besuchen, https://repaircafe.org/de/besuchen/ (Zugriff: 30.12.2021)                                                                                                                                  |
| Stichting Repair Café - FAQ (2021)                                                                     | Stichting Repair Café - FAQ, https://repaircafe.org/de/faq/ (Zugriff: 30.12.2021)                                                                                                                                            |
| Stichting Repair Café - Selbst starten (2021)                                                          | Stichting Repair Café - Selbst starten, https://repaircafe.org/de/mitmachen/selbst-starten/ (Zugriff: 02.01.2022)                                                                                                            |
| Stichting Repair Café - Über (2021)                                                                    | Stichting Repair Café - Über, https://repaircafe.org/de/ueber/ (Zugriff: 30.12.2021)                                                                                                                                         |
| Stiftung Warentest - Repair Cafés: Handys unter Anleitung selbst reparieren – funktioniert das? (2016) | Stiftung Warentest - Repair Cafés: Handys unter Anleitung selbst reparieren – funktioniert das?, https://www.test.de/Repair-Cafes-Handys-unter-Anleitung-selbst-reparieren-funktioniert-das-5019052-0/ (Zugriff: 30.12.2021) |
| Verbraucherzentrale - Repair-Cafés (2021)                                                              | Verbraucherzentrale - Repair-Cafés, https://www.verbraucherzentrale.de/wissen/umwelt-haushalt/nachhaltigkeit/repaircafes-8208 (Zugriff: 30.12.2021)                                                                          |
| Westfalen-Blatt (2022)                                                                                 | Westfalen-Blatt, https://www.westfalen-blatt.de/ (Zugriff: 02.01.2022)                                                                                                                                                       |
| Wickham, Hadley/Grolemund, Garrett (2017)                                                              | R for Data Science: Import, Tidy, Transform, Visualize, and Model Data, https://r4ds.had.co.nz/ (Zugriff: 13.02.2022)                                                                                                        |
| Wikipedia - IKEA-Effekt (2011)                                                                         | Wikipedia - IKEA-Effekt, https://de.wikipedia.org/wiki/IKEA-Effekt (Zugriff: 22.02.2022)                                                                                                                                     |


# Versicherung

"Ich versichere, dass ich die vorstehende Arbeit selbständig angefertigt und mich fremder Hilfe nicht bedient habe. Alle Stellen, die wörtlich oder sinngemäß veröffentlichtem oder nicht veröffentlichtem Schrifttum entnommen sind, habe ich als solche kenntlich gemacht."

Michael Prange
