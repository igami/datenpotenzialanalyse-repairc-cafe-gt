
## Wie können neue Reparatuere gewonnen werden?

Gamification?
- first thing of this kind!
- turn off the light (Sicherung ist raus geflogen und ein Bereich der Stadtbibliothek war dunkel)

## Datenaufbereitung

- Nutzeranalyse
- Geräteanalyse


## Wie können die Daten aufbereitet werden damit

- die Gäste sehen, dass eine Reparatur wahrscheinlich ist
- die reparateuere motiviert sind, weil viel geschafft, gelernt wurde

## Welche Informationen wären noch interessant?

- Befragung der Reparateure
- Befragung der Gäste
- Befragung der Bibliothek

## Welche Werbung ist erfolgreich?

## Welche Daten liegen vor:

- Reparaturen
  - Gerät
  - Erfolgreich
  - Fehler
  - Beschreibung
  - Datum
- Spendenaufkommen

## Wie lassen sich die Daten einfach erfassen? 

- Laufzettel Scannen/Fotografieren + Texterkennung
- Digital erfassen
  - schwierig für alle teilnehmer
  - ggf. hat die Bibliothek iPads die genutzt werden dürfen

## Wie lange dauert eine Reparatur?

## wann melden sich die Gäste an (Planungssicherheit)

## weitere Notizen zu dem Reparaturtermin

- Sondertermin
- Weshalb ausgefallen
- weshalb verschoben

## lässt sich eine Reparaturanleitung für Geräte ableiten? 

- Fehler nachstellen
- Stromversorgung prüfen
- bisection


## Schreibstil

- Reparierende, Reparateur, Mitarbeiter vereinheitlichen
- Gegenstände oder Geräte?
- Gast, Gästin, Gäste, Kunden vereinheitlichen


## weitere Notizen

- Aufwand bewerten
- Predictive Maintenance
- GitHub Copilot
- Reparierende Characterbogen
  - Expertise (10-20-40-80-160)
  - Analyse - Fehler gefunden
  - Neugierde - erste Geräte dieser Art